# Image Alignment Program #

This repository contains a Java script that runs on ImageJ. The script performs alignment or registration of image stacks using the ImageJ StackReg and TurboReg plugins.

## Dependencies ##

ImageJ must be downloaded and the StackReg and TurboReg plugins must be installed.
